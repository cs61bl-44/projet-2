import java.util.*;

public class LineNumber {
	private ArrayList<Integer> myArray = new ArrayList<Integer>();
	
	//Invalid arguments are checked in the parse method of the Proof class
	public LineNumber(String s){
		String[] splitedNum = s.split("\\.");
		for(String num : splitedNum){
			myArray.add(Integer.parseInt(num));
		}
	}
	
	public LineNumber(LineNumber ln){
		for (int num : ln.myArray){
			this.myArray.add(num);
		}
	}
	
	public String toString(){
		String str = "";
		for(int num : myArray){
			str += num + ".";
		}
		return str.substring(0, str.length() - 1);
	}
	
	public ArrayList<Integer> getArray(){
		return myArray;
	}
	
	// 1.3 => 1.4
	public void nextLine(){
		int lastIndex = myArray.size() - 1;
		myArray.set(lastIndex, myArray.get(lastIndex) + 1);
	}
	
	// 1.3 => 1.3.1
	public void nextSubLine(){
		myArray.add(1);
	}
	
	// 1.3.2 => 1.4
	public void prevLine(){
		if(myArray.size() <= 1){
			try{
				throw new IllegalStateException("Cannot call previous line on " + this.toString());
			}
			catch(IndexOutOfBoundsException e){
				throw new IllegalStateException("Cannot call previous line on empty line");
			}
		}
		int lastIndex = myArray.size() - 1;
		myArray.remove(lastIndex); //remove the last one
		nextLine();
	}
	
	public boolean equals(Object obj){
		try{
			return myArray.equals(((LineNumber)obj).myArray);
		} catch (ClassCastException ex){
			return false;
		}
	}
	
}
