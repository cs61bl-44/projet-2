1) About the organization in the group.

We made a first meeting to split the work between each member of the group following this suggestion :

    a) one kind of error checking, say, checking the syntax of a line or detecting wrong line numbers;
    b) parsing an expression;
    c) at least two of checking isComplete, managing line numbers, and implementing toString;
    d) making an inference (once you can handle modus ponens, the rest are straightforward);
    e) handling theorems.

a) + b) => Pierre
c) Jocelyn
d) Sajith
e) Melissa

After having begun to work on the code itself, it has changed a bit so that Sajith implemented isComplete and toString for Proof because it was linked with the inference handling. Pierre took also the LineNumber class and the exprHelper method in Expression. Jocelyn made all the rest of the Expression class to check all kinds of errors. Melissa coded the entirety of TheoremSet and the matchesTheorem method in Expression and made a lot of changes to organize the code.  The most part of the work was made in class so that we could discuss together about problems.
We often stayed late as a group. All 4 members shared in debugging all code.  Everyone has participated a lot to this project, we could trust others about their work and this is the most important in such a project with 4 partners. The code was commented and although it took a certain effort to be able to use git as version manager, it was useful and worked well.



2) About testing.

Each class has its own test file that test only its methods. For example the parse test checks only if the parsing work and not if the Expression is valid. Here was made a small exception for valid LineNumber checking. To avoid having to implement a new loop after the parsing and to make the implementation easier, LineNumbers are directly checked in the parse method and created before being inserted in an ArrayList<LineNumber>.
Testing like this is really efficient. When some method use another class, if this class has been tested correctly, the user will immediately see if his own methods respect the standards. At the end if all tests of all test classes pass, the program should work correctly. "Human" tests would even maybe not be necessarily to do when the project is finished.
*Details about rationale of the tests will be found directly in the tests files for each test.*