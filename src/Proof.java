import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Proof extends BinaryTree{
	
	/*
	 * Each statement in the proof is represented by a
	 * ProofNode.
	 * 
	 * lastStatement points to the last line in the proof 
	 * so far. If the last line completes a subproof, then
	 * lastStatement points to the first statement before 
	 * the subproof.
	 *  
	 * myHead points to a sentinel node that points to the
	 * first statement in the proof.
	 * 
	 * showNodes contains the show statements that have yet
	 * to be proven, with the most recent on the top of the
	 * stack.
	 * 
	 * currentLine is the LineNumber of the next statement
	 * to be input.
	 * 
	 * done is true if the proof is completed, false otherwise
	 */
	private TheoremSet myTheorems;
	private ProofNode lastStatement, myHead;
	private Stack<ProofNode> showNodes;
	private LineNumber currentLine;
	private boolean done;

	//Proof constructor
	public Proof (TheoremSet theorems) {
		showNodes = new Stack<ProofNode>();
		myTheorems = theorems;
		lastStatement = myHead = new ProofNode();
		currentLine = new LineNumber("1");
		done = false;
	}

	//returns LineNumber for next input statement
	public LineNumber nextLineNumber ( ) {
		return currentLine;
	}

	/*return an ArrayList<Object> with indexes :
	* 0 : reason or theorem
	* 1 : expression
	* 2 : ArrayList<LineNumber> with the line numbers for mp
	* 
	* This checks wrong inputs in general and wrong inputs for LineNumbers as argument for mp, mq, mt, ic.
	* Invalid expression are checked in the Expression class
	* Invalid number of LineNumber are checked in the extendProof method.
	*/
	private static ArrayList<Object> parse(String x) throws IllegalLineException, PrintException{
		x = x.trim();
		ArrayList<Object> rtn = new ArrayList<Object>();
		ArrayList<LineNumber> inner_rtn = new ArrayList<LineNumber>();
		
		//special case for print
		if (x.equals("print")){
			throw new PrintException();
		}
		
		Matcher outer = Pattern.compile("^(\\S+) +(((\\d+\\.?)+) +){0,2}(\\S+){1}$").matcher(x);
		Matcher inner;
		while (outer.find()) {
			rtn.add(outer.group(1)); //reason			
			rtn.add(outer.group(5)); //expression
			inner = Pattern.compile("((\\d+\\.?)+)").matcher(x);			
			inner.region(0, outer.end());
			while(inner.find()){
				if(inner.group(0).endsWith(".")){
					throw new IllegalLineException("Invalid Input"); // handle the invalid case : 1.2.3.
				}
				inner_rtn.add(new LineNumber(inner.group(0))); //line numbers
			}
			rtn.add(inner_rtn);
		}
		if(!outer.matches()){
			throw new IllegalLineException("Invalid Input"); // if nothing was matched, the input in not valid
		}
		return rtn;
	}
	
	//public method to test the parse method from ProofTest
	public ArrayList<Object> parseTest(String x) throws IllegalLineException{
		try{
			return parse(x);
		} catch (PrintException ex){
			return null;
		}
	}
	
	/*
	 * if x is a valid statement, adds the statement to the proof
	 * if x is print, prints the statements in the proof so far
	 * otherwise throws an exception
	 */
	public void extendProof (String x) throws IllegalLineException, IllegalInferenceException {
		//Parse the String x into a reason, LineNumbers, and an expression
		ArrayList<Object> parsedString;
		
		// special case for print
		try{
			parsedString = parse(x);
		} catch (PrintException ex){
			System.out.println(this.toString());
			return;
		}
			
		String reason = (String) parsedString.get(0);
		Expression expr = new Expression((String) parsedString.get(1));
		ArrayList<LineNumber> lines = (ArrayList<LineNumber>) parsedString.get(2);
		
		//Check if the given LineNumbers are valid
		//If a LineNumber is invalid, throws IllegalLineException
		ArrayList<Expression> givens = checkLineNumbers(reason, lines);
		
		//Check if the inference is valid; or print the proof
		//If the inference is invalid, throws IllegalInferenceException
		if (reason.equals("assume")){
			checkAssume(expr);
		} else if (reason.equals("mp")){
			checkMP(givens, expr);
		} else if (reason.equals("mt")){
			checkMT(givens, expr);
		} else if (reason.equals("co")){
			checkCO(givens, expr);
		} else if (reason.equals("ic")){
			checkIC(givens, expr);
		} else if (reason.equals("repeat")){
			checkRepeat(givens, expr);
		} else if (!reason.equals("show")){
			checkTheorem(reason, expr);
		}	
		
		//Add the statement as a ProofNode
		//If the statement was a 'show', add it to showNodes
		lastStatement = new ProofNode(reason, lines, expr, lastStatement);
		if (reason.equals("show"))
			showNodes.push(lastStatement);
		update();
		
	}
	
	/*
	 * Checks whether ArrayList lines contains the correct number
	 * of LineNumbers for the corresponding reason.
	 * Also checks if the LineNumbers are accessible from current
	 * position in proof.
	 * Returns ArrayList of Expressions corresponding to the given
	 * line numbers.
	 * Throws an IllegalLineException if either check fails.
	 */
	private ArrayList<Expression> checkLineNumbers(String reason, ArrayList<LineNumber> lines) throws IllegalLineException{
		ArrayList<Expression> exps = new ArrayList<Expression>();
		int numLines = lines.size();
		try{
			switch (reason){
				case "ic":
				case "repeat":
					if (numLines != 1)
						throw new IllegalLineException("*** "+reason+" requires 1 line number");
					exps.add(lastStatement.getExpOfLine(lines.get(0)));
					break;
				case "mp":
				case "mt":
				case "co":
					if (numLines != 2)
						throw new IllegalLineException("*** "+reason+" requires 2 line numbers");
					exps.add(lastStatement.getExpOfLine(lines.get(0)));
					exps.add(lastStatement.getExpOfLine(lines.get(1)));
					break;
				default:
					if (numLines != 0)
						throw new IllegalLineException("*** "+reason+" requires no line numbers");
					break;
			}
		} catch (NoSuchElementException ex){
			throw new IllegalLineException("Inaccessible Line: "+ex.getMessage());
		}
		return exps;
	}
	
	//Checks that expr is a valid assumption
	//throw IllegalInferenceException otherwise
	private void checkAssume(Expression expr) throws IllegalInferenceException{
		if (!lastStatement.reason.equals("show")){
			throw new IllegalInferenceException("*** assume can only follow show statement");
		}
		try{
			if (!expr.equals(new Expression("~"+lastStatement.expr))){
				if (!lastStatement.expr.getExpOperator().equals("=>")){
					throw new IllegalInferenceException("*** invalid assumption: can only assume ~E from E; or E from (E=>D)");
				}
				if (!expr.equals(lastStatement.expr.getLeftExpression())){
					throw new IllegalInferenceException("*** invalid assumption: can only assume ~E from E; or E from (E=>D)");
				}
			}
		} catch (IllegalLineException ex){
			throw new IllegalStateException("Illegal Expression present in proof: " + expr);
		}		
	}
	
	//Checks that expr follows from Expressions in ArrayList givens, by modus ponens
	//throws IllegalInferenceException otherwise
	private void checkMP(ArrayList<Expression> givens, Expression expr) throws IllegalInferenceException{
		Expression given1 = givens.get(0);
		Expression given2 = givens.get(1);
		Expression implication1, implication2;
		
		try{
			implication1 = new Expression("("+given1+"=>"+expr+")");
			implication2 = new Expression("("+given2+"=>"+expr+")");
		} catch (IllegalLineException ex){
			throw new IllegalStateException("Illegal Expression present in proof "+given1+" "+given2+" "+expr);
		}
		if (!implication1.equals(given2) && !implication2.equals(given1)){
			throw new IllegalInferenceException("*** bad inference: can only infer q from p and (p=>q) by mp");
		}
	}
	
	//Checks that expr follows from Expressions in ArrayList givens, by modus tollens
	//throws IllegalInferenceException otherwise
	private void checkMT(ArrayList<Expression> givens, Expression expr) throws IllegalInferenceException{
		Expression given1 = givens.get(0);
		Expression given2 = givens.get(1);
		Expression implication1, implication2;
		
		String given1Type = given1.getExpOperator();
		String given2Type = given2.getExpOperator();
		String exprType = expr.getExpOperator();
		
		//first check that expr is a negation, and that one of the two
		//other expressions is a negation
		if (!(exprType.equals("~") && (given1Type.equals("~") || given2Type.equals("~")))){
			throw new IllegalInferenceException("*** bad inference: can only infer ~p from ~q and (p=>q) by mt");
		}
		
		try{
			implication1 = new Expression("("+expr.getRightExpression()+"=>"+given1.getRightExpression()+")");
			implication2 = new Expression("("+expr.getRightExpression()+"=>"+given2.getRightExpression()+")");
			if (!implication1.equals(given2) && !implication2.equals(given1)){
				
				throw new IllegalInferenceException("*** bad inference: can only infer ~p from ~q and (p=>q) by mt");
			}
		} catch (IllegalLineException ex){
			throw new IllegalStateException("Illegal Expression present in proof: "+given1+" "+given2+" "+expr);
		}
	}
	
	//Checks that expr follows from Expressions in ArrayList givens, by contradiction
	//throws IllegalInferenceException otherwise
	private void checkCO(ArrayList<Expression> givens, Expression expr) throws IllegalInferenceException{
		Expression given1 = givens.get(0);
		Expression given2 = givens.get(1);
		boolean valid = false;
		
		if (given1.getExpOperator().equals("~"))
			if (given1.getRightExpression().equals(given2))
				valid = true;
			
		if (given2.getExpOperator().equals("~"))
			if (given2.getRightExpression().equals(given1))
				valid = true;
			
		if (!valid) 
			throw new IllegalInferenceException("*** bad inference: co requires expressions E and ~E to make an inference");
	}
	
	//Checks that expr follows from Expressions in ArrayList givens, by implication construction
	//throws IllegalInferenceException otherwise
	private void checkIC(ArrayList<Expression> givens, Expression expr) throws IllegalInferenceException{
		Expression given = givens.get(0);
		
		if (!expr.getExpOperator().equals("=>"))
			throw new IllegalInferenceException("*** bad inference: ic can only infer a conditional");
		
		if (!expr.getRightExpression().equals(given))
			throw new IllegalInferenceException("*** bad inference: ic can only infer a=>b from b");
	}
	
	//The expression in ArrayList givens, is the expression found by
	//checkLineNumbers(). Checks that it is equivalent to expr, and
	//can thus be repeated.
	//If it is not equivalent, throws IllegalInferenceException
	private void checkRepeat(ArrayList<Expression> givens, Expression expr) throws IllegalInferenceException{
		Expression given = givens.get(0);
		
		if (!given.equals(expr))
			throw new IllegalInferenceException("*** bad inference: expression on line is different from that in repeat");
	}
	
	//Checks that the theorem of the given name matches the Expression expr
	//If no theorem exists with that name, or the theorem doesn't match,
	//throws an IllegalInferenceException
	private void checkTheorem(String name, Expression expr) throws IllegalInferenceException, IllegalLineException{
		Expression theorem = myTheorems.mySet.get(name);
		if (theorem == null)
			throw new IllegalLineException("*** Theorem "+name+" does not exist.");
		if(!expr.matchesTheorem(myTheorems.mySet.get(name)))
			throw new IllegalInferenceException("*** "+expr+" does not match "+name+": "+theorem);
	}
	
	//Updates currentLine depending on whether the last statement
	//starts a new subproof, completed a subproof, or continues the
	//current proof.
	//Sets done to true if the proof is finished.
	private void update(){
		if (lastStatement == myHead.nextNode){
			currentLine.nextLine();		//special case for first show statement
		} else if (lastStatement.reason.equals("show")){
			currentLine.nextSubLine();	//starts subproof
		} else {
			if (lastStatement.expr.equals(showNodes.peek().expr)){
				lastStatement = showNodes.pop();
				if (showNodes.isEmpty())
					done = true;
				if (!isComplete())
					currentLine.prevLine();	//completed subproof
			} else {
				currentLine.nextLine();		//continues current proof
			}
		}
	}

	/*
	 * Each time a proof is completed, is is popped off showNodes.
	 * If showNodes is empty, all show statements have been proven.
	 * Returns true if main proof is complete, false otherwise.
	 */
	public boolean isComplete ( ) {
		return done;
	}
	
	// Returns string of proof statements, with statements separated by newlines.
	public String toString(){
		return toString(myHead);
	}
	
	// Helper method for toString()
	private String toString(ProofNode node){
		String result = node.toString();
		if (node.subProof != null)
			result += "\n"+toString(node.subProof);
		if (node.nextNode != null)
			result += "\n"+toString(node.nextNode);
		return result;
	}
	
	//Each ProofNode represents a single statement in the proof
	private class ProofNode{
		private LineNumber lineNum;  //line number of proof statement
		private String reason;       //the inference rule or theorem name 
		private ArrayList<LineNumber> givens; //the previous lines used by the inference
		private Expression expr;     //the expression derived in the statement
		private ProofNode prevNode;  //the previous statement in the proof
		private ProofNode subProof;  //the start of a subproof on the following statement
		private ProofNode nextNode;  //the next statement in the current proof
		
		
		//Constructor for sentinel node of Proof
		public ProofNode(){
			lineNum = null;
			reason = "";
			givens = null;
			expr = null;
			prevNode = subProof = nextNode = null;
		}
		
		//constructor for normal proof nodes
		public ProofNode(String rsn, ArrayList<LineNumber> giv, Expression ex, ProofNode prev){
			reason = rsn;
			givens = giv;
			expr = ex;
			prevNode = prev;
			subProof = nextNode = null;
			lineNum = new LineNumber(currentLine);
			if (showNodes.contains(prev)){
				prev.subProof = this;
			} else {
				prev.nextNode = this;
			}
			
		}
		
		//returns first node of subproof that begins on following line
		public ProofNode getSubProof(){
			if (subProof == null){
				throw new NoSuchElementException();
			}
			return subProof;
		}
		
		//returns node of next statement in current proof
		public ProofNode getNextNode(){
			if (nextNode == null){
				throw new NoSuchElementException();
			}
			return nextNode;
		}
		
		//returns String representation of ProofNode
		//ex: 3.2.2	   mp 2 3.2.1 (~p=>q)
		public String toString(){
			String result = "";
			if(this==myHead)
				return "";
			for (LineNumber x : givens){
				result = result + x + " ";
			}
			return lineNum + "\t" + reason + " " + result + expr; 
		}
		
		//returns the Expression proved in LineNumber ln
		//checks the current ProofNode, then checks the node
		//corresponding to the previous statement in the proof
		//if the line matching ln is not found, throws NoSuchElementException
		public Expression getExpOfLine(LineNumber ln){
			if (this == myHead){
				throw new NoSuchElementException(ln.toString());
			} else if (ln.equals(lineNum)){
				if (reason.equals("show"))
					 if (showNodes.contains(this))
						 return prevNode.getExpOfLine(ln);
				return expr;
			} else {
				return prevNode.getExpOfLine(ln);
			}
		}
		
	}
	
	// thrown by parse() method to indicate a print command
	private static class PrintException extends Exception{
	}
}
