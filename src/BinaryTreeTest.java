import static org.junit.Assert.*;

import org.junit.Test;


public class BinaryTreeTest {

//	tests general construction of a BinaryTree object
//	if this were to fail, we would need to examine the base code for a binary tree
	@Test
	public void testConstructor( ) {
		BinaryTree t1 = new BinaryTree( );
		assertEquals(t1.getRoot( ), null);
		TreeNode a = new TreeNode("A");
		BinaryTree t2 = new BinaryTree(a);
		assertTrue(t2.getRoot( ).getItem( ).equals("A"));
		assertFalse(t2.getRoot( ).getItem( ).equals("B"));
	}
	
//	tests equailty of trees (specifically expression trees)
//	if this test were to fail, we would need to look at how we are testing equality
//	between two tree nodes.
	@Test
	public void testEquals() {
		BinaryTree tree1, tree2;
		TreeNode node;
		// if the arg tree is null
		tree1 = new BinaryTree();
		tree2 = null;
		assertFalse(tree1.equals(tree2));
		
		// if both trees are empty
		tree2 = new BinaryTree();
		assertTrue(tree1.equals(tree2));
		
		// if both trees have 1 element
		node = new TreeNode("a");
		tree1 = new BinaryTree(node);
		node = new TreeNode("a");
		tree2 = new BinaryTree(node);
		assertTrue(tree1.equals(tree2));
		node = new TreeNode("b");
		tree2 = new BinaryTree(node);
		assertFalse(tree1.equals(tree2));
		
		// if trees contain multiple elements
		node = new TreeNode("=>",
				new TreeNode("a"),
				new TreeNode("b"));
		tree1 = new BinaryTree(node);
		node = new TreeNode("=>",
				new TreeNode("a"),
				new TreeNode("b"));
		tree2 = new BinaryTree(node);
		assertTrue(tree1.equals(tree2));
		node = new TreeNode("=>",
				new TreeNode("b"),
				new TreeNode("a"));
		tree2 = new BinaryTree(node);
		assertFalse(tree1.equals(tree2));
		
		node = new TreeNode("=>",
				new TreeNode("&",
				  new TreeNode("a"),
				  new TreeNode("|",
					new TreeNode("c"),
					new TreeNode("b"))),
				new TreeNode("~",
				  null,
				  new TreeNode("d")));
		tree1 = new BinaryTree(node);
		node = new TreeNode("=>",
				new TreeNode("&",
				  new TreeNode("a"),
				  new TreeNode("|",
					new TreeNode("c"),
					new TreeNode("b"))),
				new TreeNode("~",
				  null,
				  new TreeNode("d")));
		tree2 = new BinaryTree(node);
		assertTrue(tree1.equals(tree2));
		node = new TreeNode("=>",
				new TreeNode("&",
				  new TreeNode("a"),
				  new TreeNode("|",
					new TreeNode("c"),
					new TreeNode("d"))),
				new TreeNode("~",
				  null,
				  new TreeNode("d")));
		tree2 = new BinaryTree(node);
		assertFalse(tree1.equals(tree2));
		node = new TreeNode("=>",
				new TreeNode("&",
				  new TreeNode("a"),
				  new TreeNode("|",
					new TreeNode("c"),
					new TreeNode("b"))),
				new TreeNode("~",
				  null,
				  new TreeNode("d",
					null,
					new TreeNode("d"))));
		tree2 = new BinaryTree(node);
		assertFalse(tree1.equals(tree2));
	}
	
//	tests method from tree to string
//	if this were to fail, we would need to examine the getExpString method
	@Test
	public void testGetExpString(){
		BinaryTree tree = new BinaryTree();
		TreeNode node;
		// if tree is empty
		assertEquals("", tree.getExpString());
		
		// if tree has 1 element
		node = new TreeNode("a");
		tree = new BinaryTree(node);
		assertEquals("a", tree.getExpString());
		
		//if tree has multiple elements
		node = new TreeNode("&", 
				new TreeNode("a"), 
				new TreeNode("b"));
		tree = new BinaryTree(node);
		assertEquals("(a&b)", tree.getExpString());
		
		node = new TreeNode("&",
				new TreeNode("a"),
				new TreeNode("=>",
					new TreeNode("b"),
					new TreeNode("c")));
		tree = new BinaryTree(node);
		assertEquals("(a&(b=>c))", tree.getExpString());
		
		node = new TreeNode("&",
				new TreeNode("a"),
				new TreeNode("=>",
					new TreeNode("b"),
					new TreeNode("&",
					  new TreeNode("b"),
					  new TreeNode("c"))));
		tree = new BinaryTree(node);
		assertEquals("(a&(b=>(b&c)))", tree.getExpString());
	}
	
//	tests if size method works
//	if this were to fail, we would need to examine the size method
	@Test
	public void testSize(){
		BinaryTree tree;
		TreeNode node;
		//test empty tree
		tree = new BinaryTree();
		assertEquals(0, tree.size());
		
		//test tree with 1 element
		node = new TreeNode("a");
		tree = new BinaryTree(node);
		assertEquals(1, tree.size());
		
		//test tree with multiple nodes
		node = new TreeNode("&",
				 new TreeNode("a"),
				 new TreeNode("|",
					new TreeNode("~",
						null,
						new TreeNode("c")),
					new TreeNode("=>",
						new TreeNode("d"),
						new TreeNode("e"))));
		tree = new BinaryTree(node);
		assertEquals(8, tree.size());
	}

}
