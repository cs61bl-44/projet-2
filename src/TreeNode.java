public class TreeNode {
		
		//The methods of this class are tested by the BinaryTreeTest class.
	
		public Object myItem;
		public TreeNode myLeft;
		public TreeNode myRight;
		
		public TreeNode (Object obj) {
			myItem = obj;
			myLeft = myRight = null;
		}
		
		public TreeNode (Object obj, TreeNode left, TreeNode right) {
			myItem = obj;
			myLeft = left;
			myRight = right;
		}
		
		public boolean hasChild(){
			if(this.myLeft == null && this.myRight == null){
				return false;
			}
			return true;
		}
		
		public Object getItem(){
			return myItem;
		}
		
		public TreeNode getLeft(){
			return myLeft;
		}
		
		public TreeNode getRight(){
			return myRight;
		}
		
		public String toString(){
			String left = "", right = "", result;
			if (myLeft != null)
				left += myLeft;
			if (myRight != null)
				right += myRight;
			result = left + myItem + right;
			if ("=>&|".contains(myItem.toString()))
				result = "("+result+")";
			return result;
		}
	}