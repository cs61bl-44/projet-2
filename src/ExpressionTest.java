import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import junit.framework.TestCase;

public class ExpressionTest extends TestCase {
	
	@Test
	public void testConstructor( ){
		try {
			BinaryTree tree;
			TreeNode node;
			
//			 single variable expression
//			if it were to fail, we would need to look at the implementation of BinaryTree within Expression
			node = new TreeNode("a");
			tree = new BinaryTree(node);
			assertEquals(tree, new Expression("a").getTree( ));
	
//			 single variable expression with ~
//			if it were to fail, we would need to look at the implementation of BinaryTree within Expression
//			Specifically, we would need to inquire about recognizing ~ as an operator
			node = new TreeNode("~",
					null,
					new TreeNode("a"));
			tree = new BinaryTree(node);
			assertEquals(tree, new Expression("~a").getTree( ));
	
			// single variable expression with ~~
//			if it were to fail, we would need to look at either the parser, the exception throwing
//			in case one was being thrown unnecessarily, or the way BinaryTree is implemented and 
//			how it recognizes operators.
			node = new TreeNode("~",
					null,
					new TreeNode("~", null, new TreeNode("a")));
			tree = new BinaryTree(node);
			assertEquals(tree, new Expression("~~a").getTree( ));
			
			// simple expression
//			if it were to fail, we would need to look at the implementation of BinaryTree within Expression
//			Specifically, we would need to inquire about recognizing => as an operator
			node = new TreeNode("=>",
					new TreeNode("a"),
					new TreeNode("b"));
			tree = new BinaryTree(node);
			assertEquals(tree, new Expression("(a=>b)").getTree( ));
	
			
			// compound expression and =>~
//			if this were to fail, we would need to look at the BinairyTree implementation in general
//			and the error handling.  Passing this test proves that more complex expression can be
//			evaluated.  To some extent, this is the "normal case."
			node = new TreeNode("=>",
					new TreeNode("&",
					  new TreeNode("a"),
					  new TreeNode("|",
						new TreeNode("c"),
						new TreeNode("d"))),
					new TreeNode("~",
					  null,
					  new TreeNode("&",
						new TreeNode("a"),
						new TreeNode("b"))));
			tree = new BinaryTree(node);
			assertEquals(tree, new Expression("((a&(c|d))=>~(a&b))").getTree( ));
			
			//test with &~
//			if this were to fail, we would need to check the expression parser (binary 
//			tree searching) and the implementation of Binary Tree to ensure the negation 
//			is being treated correctly.  We had some problems with this earlier and explicitly
//			wrote tests to isolate the issue.
			node = new TreeNode("=>",new TreeNode("&", new TreeNode("a"),new TreeNode("~",null,new TreeNode("c"))), new TreeNode("|", new TreeNode("a"), new TreeNode("b")));
			tree = new BinaryTree(node);
			assertEquals(tree, new Expression("((a&~c)=>(a|b))").getTree( ));
			
			//test with |~
//			if this were to fail, we would need to check the expression parser (binary 
//			tree searching) and the implementation of Binary Tree to ensure the negation 
//			is being treated correctly.  We had some problems with this earlier and explicitly
//			wrote tests to isolate the issue.
			node = new TreeNode("=>",new TreeNode("|", new TreeNode("a"),new TreeNode("~",null,new TreeNode("c"))), new TreeNode("|", new TreeNode("a"), new TreeNode("b")));
			tree = new BinaryTree(node);
			assertEquals(tree, new Expression("((a|~c)=>(a|b))").getTree( ));
			
			//test with &~
//			if this were to fail, we would need to check the expression parser (binary 
//			tree searching) and the implementation of Binary Tree to ensure the negation 
//			is being treated correctly.  We had some problems with this earlier and explicitly
//			wrote tests to isolate the issue.
			node = new TreeNode("=>",new TreeNode("&", new TreeNode("a"),new TreeNode("~",null,new TreeNode("c"))), new TreeNode("|", new TreeNode("a"), new TreeNode("b")));
			tree = new BinaryTree(node);
			assertEquals(tree, new Expression("((a&~c)=>(a|b))").getTree( ));
		}
		catch(IllegalLineException ile) {
			System.err.println(ile.getMessage( ));
		}
	}
	
//	Testing need for ()
//	if this test were to fail, we would need to look at the expression parser (binary
//	tree searching) and the implementation of IllegalLineException to ensure an exception
//	is being thrown when () are needed.
	public void testException () {
		boolean output = false;
		try {
			Expression test = new Expression("a|b");
		}
		catch (IllegalLineException e) {
			System.out.println(e.getMessage());
			output = true;
		}
		assertTrue(output);
	} 

//	testing if exception is thrown when a variable is more than 1 character
//	if this test were to fail, we would need to look at how variables are
//	managed within the expression (represented as a binary tree)
	public void testVar () {
		boolean output = false;
		try {
			Expression test = new Expression("(ab&c)");
		}
		catch (IllegalLineException e) {
			output = true;
			System.out.println(e.getMessage());
		}
		assertTrue(output);
	}
	
//	test the normal case
//	if this were to fail, we would need to look at the exceptions, the implementation
//	of binary tree within expression, and the mannner in which the expression is evaluated
	public void testValid () {
		boolean output = false;
		try {
			Expression test = new Expression ("(a&b)");
		}
		catch (IllegalLineException e) {
			output = true;
		}
		assertFalse(output);
	}
	
//	test a more complex normal case
//	if this were to fail, we would need to look at the exceptions, the implementation
//	of binary tree within expression, and the mannner in which the expression is evaluated
	public void testValid2 () {
		boolean output = false;
		try {
			Expression test = new Expression ("((a&b)|~d)");
		}
		catch (IllegalLineException e) {
			System.out.println(e.getMessage());
			output = true;
		}
		assertFalse(output);
	}
	
//	Testing a nested negation
//	we had problems with nested negations and needed to isolate the issue.  This test helped
//	us determine what was wrong with the evaluation of the expression.
	public void testValid3 () {
		boolean output = false;
		try {
			Expression test = new Expression ("(c|~d)");
		}
		catch (IllegalLineException e) {
			System.out.println(e.getMessage());
			output = true;
		}
		assertFalse(output);
	}
	
//	Testing lack of nested negation
//	we had problems with nested negations and needed to isolate the issue.  This test helped
//	us determine what was wrong with the evaluation of the expression.
	public void testValid4 () {
		boolean output = false;
		try {
			Expression test = new Expression ("((a&b)|d)");
		}
		catch (IllegalLineException e) {
			System.out.println(e.getMessage());
			output = true;
		}
		assertFalse(output);
	}
	
//	test exception thrown when spaces included within expression
//	If this test were to fail, we would need to look at the exception that detects spaces
//	in expressions
	public void testSpace () {
		boolean output = false;
		try {
			Expression test = new Expression ("((a | b) | p)");
		}
		catch (IllegalLineException e) {
			System.out.println(e.getMessage());
			output = true;
		}
		assertTrue(output);
	}
	
//	test exception that detects unnecessary ()
//	if this were to fail, we would need to look at under what conditions the exception
//	that detects unnecessary () is thrown
	public void testBadVar () {
		boolean output = false;
		try {
			Expression test = new Expression ("(a)");
		}
		catch (IllegalLineException e) {
			System.out.println(e.getMessage());
			output = true;
		}
		assertTrue(output);
	}
	
//	test a proper variable
//	if this test were to fail, we would need to look at the exception thrown
//	necessitating () for a certain expression
	public void testGoodVar () {
		boolean output = false;
		try {
			Expression test = new Expression ("a");
		}
		catch (IllegalLineException e) {
			System.out.println(e.getMessage());
			output = true;
		}
		assertFalse(output);
	}
	
//	tests the exception thrown when () are not needed
//	if this test were to fail, we would need to look the exception thrown
//	necessitating () for a certain expression as well as how negated variables
//	are evaluated
	public void testBadNot () {
		boolean output = false;
		try {
			Expression test = new Expression ("(~a)");
		}
		catch (IllegalLineException e) {
			System.out.println(e.getMessage());
			output = true;
		}
		assertTrue(output);
	}
	
//	tests valid negated expressions
//	if this test were to fail, we would need to look at how negations are handled
//	when expressions are turned into binary trees
	public void testNot () {
		boolean output = false;
		try {
			Expression test = new Expression ("~a");
		}
		catch (IllegalLineException e) {
			System.out.println(e.getMessage());
			output = true;
		}
		assertFalse(output);
		try {
			Expression test2 = new Expression ("~~a");
		}
		catch (IllegalLineException e) {
			System.out.println(e.getMessage());
			output = true;
		}
		assertFalse(output);
	}
	
//	tests simplification of Expression within matchesTheorem
//	if this were to fail, (but all previous tests were to pass) we
//	would need to look at the matchesTheorem method as well as how
//	& is evaluated.  If all following tests fail, we would need to
//	examine how => is evaluated.
	public void testMatchesTheorem1( ) {
		try {
			Expression and1 = new Expression("((x&y)=>x)");
			// Normal input
			Expression line11 = new Expression("((a&b)=>a)");
			assertTrue(line11.matchesTheorem(and1));
			// Normal input with more complex sub-expressions
			Expression line12 = new Expression("(((a|b)&~c)=>(a|b))");
			assertTrue(line12.matchesTheorem(and1));
			// Incorrect input with same variables, different implication
			Expression line13 = new Expression("((a&b)=>b)");
			assertFalse(line13.matchesTheorem(and1));
			Expression line14 = new Expression("(((a|b)&~c)=>~c)");
			assertFalse(line14.matchesTheorem(and1));
			// Incorrect input with same variables, different operator
			Expression line15 = new Expression("((a|b)=>b)");
			assertFalse(line15.matchesTheorem(and1));
			Expression line16 = new Expression("(((a|b)|~c)=>(a|b))");
			assertFalse(line16.matchesTheorem(and1));
			// Incorrect input with different variables
			Expression line17 = new Expression("((x&y)=>c)");
			assertFalse(line17.matchesTheorem(and1));
			Expression line18 = new Expression("(((a|d)&~c)=>(a|b))");
			assertFalse(line18.matchesTheorem(and1));
		}
		catch (IllegalLineException ile) {
			System.err.println(ile.getMessage( ));
			fail();
		}
	}
	
//	tests simplification of Expression within matchesTheorem
//	if this were to fail, (but all previous tests were to pass) we 
//	would need to look at the matchesTheorem method as well as how
//	multiple negations are evaluated
	public void testMatchesTheorem2() {
		try {
			Expression dn = new Expression("(~~x=>x)");
			// Normal input
			Expression line21 = new Expression("(~~a=>a)");
			assertTrue(line21.matchesTheorem(dn));
			// Normal input with more complex sub-expressions
			Expression line22 = new Expression("(~~(a&b)=>(a&b))");
			assertTrue(line22.matchesTheorem(dn));
			// Incorrect input with same variables, different implication
			Expression line23 = new Expression("(~~(a&b)=>(a&a))");
			assertFalse(line23.matchesTheorem(dn));
			// Incorrect input with same variables, different operator
			Expression line24 = new Expression("(~~(a&b)=>(a|b))");
			assertFalse(line24.matchesTheorem(dn));
			// Incorrect input with different variables
			Expression line25 = new Expression("(~~(a&b)=>(c&b))");
			assertFalse(line25.matchesTheorem(dn));
		}
		catch (IllegalLineException ile) {
			System.err.println(ile.getMessage( ));
			fail();
		}
	}
		
//	tests simplification of Expression within matchesTheorem
//	if this were to fail, (but all previous tests were to pass) we 
//	would need to look at the matchesTheorem method as well as how
//	=> and & are evaluated
	public void testMatchesTheorem3( ) {
		try {				
			Expression buildAnd = new Expression("(a=>(b=>(a&b)))");
			// Normal input
			Expression line31 = new Expression("(x=>(y=>(x&y)))");
			assertTrue(line31.matchesTheorem(buildAnd));
			// Normal input with more complex sub-expressions
			Expression line32 = new Expression("((~a&b)=>(c=>((~a&b)&c)))");
			assertTrue(line32.matchesTheorem(buildAnd));
			// Incorrect input with same variables, different implication
			Expression line33 = new Expression("(x=>(y=>y))");
			assertFalse(line33.matchesTheorem(buildAnd));
			// Incorrect input with same variables, different operator
			Expression line34 = new Expression("(x=>(y&(x&y)))");
			assertFalse(line34.matchesTheorem(buildAnd));
			// Incorrect input with different variables
			Expression line35 = new Expression("(x=>(y=>(c&y)))");
			assertFalse(line35.matchesTheorem(buildAnd));
		}
		catch (IllegalLineException ile) {
			System.err.println(ile.getMessage( ));
			fail();
		}
	}
		
//	tests simplification of Expression within matchesTheorem
//	if this were to fail, (but all previous tests were to pass) we 
//	would need to look at the matchesTheorem method as well as how
//	expressions with no => are evaluated
	public void testMatchesTheorem4( ) {
		try {	
			Expression noImplication = new Expression("(~p|p)");
			// Normal input
			Expression line41 = new Expression("(~a|a)");
			assertTrue(line41.matchesTheorem(noImplication));
			Expression line42 = new Expression("(~(a&b)|(a&b))");
			assertTrue(line42.matchesTheorem(noImplication));
			// Incorrect application of theorem
			Expression line43 = new Expression("(a|a)");
			assertFalse(line43.matchesTheorem(noImplication));
			// Incorrect application of theorem
			Expression line44 = new Expression("(~a|b)");
			assertFalse(line44.matchesTheorem(noImplication));
		}
		catch (IllegalLineException ile) {
			System.err.println(ile.getMessage( ));
			fail();
		}
	}
}

