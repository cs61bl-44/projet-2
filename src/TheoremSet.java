import java.util.*;

//test

public class TheoremSet {
	
	public HashMap<String, Expression> mySet;

	public TheoremSet ( ) {
		mySet = new HashMap<String, Expression>();
	}

	public Expression put (String s, Expression e) throws IllegalLineException {
		if(s == null || e == null) {
			throw new IllegalLineException("Null theorem name");
		}
		String thrm = s.trim( );
		if(thrm.equals("")) {
			throw new IllegalLineException("Empty theorem name");
		}
		mySet.put(s, e);
		return e;
	}
}
