import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;


public class ProofTest {
//	if any of these portions were to fail, we would need to examine the code behind
//	parsedString method of the respective elements (spaces, invalid expression, without
//	line number, incorrect inputs, incorrect line number)
	@Test
	public void parseTest() {
		String reason;
		String expr;
		ArrayList<LineNumber> lines;
		Proof p = new Proof(null);
		ArrayList<Object> parsedString = new ArrayList<Object>();
		
		// test that spaces between arguments are valid
		try{
			parsedString = p.parseTest("mp     5.5        4          (((p=>q)=>q)=>((q=>p)=>p))      ");
			reason = (String) parsedString.get(0);
			expr = (String) parsedString.get(1);
			lines = (ArrayList<LineNumber>) parsedString.get(2);
			
			assertEquals("mp", reason);
			assertEquals("(((p=>q)=>q)=>((q=>p)=>p))", expr.toString());
			assertEquals("5.5", lines.get(0).toString());
			assertEquals("4", lines.get(1).toString());
		}catch (IllegalLineException e) {
            System.out.println (e.getMessage());
		}
		
		// test with one line number and that a invalid string expression "f(a|b)" is handle as valid
		// (will be then checked in the Expression class).
		try{
			parsedString = new ArrayList<Object>();
			parsedString = p.parseTest("ic 5.8.9 f(a|b)");
			reason = (String) parsedString.get(0);
			expr = (String) parsedString.get(1);
			assertEquals("f(a|b)", expr.toString());
			lines = (ArrayList<LineNumber>) parsedString.get(2);
			
			assertEquals("ic", reason);
			assertEquals("5.8.9", lines.get(0).toString());
		} catch (IllegalLineException e) {
            System.out.println (e.getMessage());
		}
		
		//test without line number
		try{
			parsedString = new ArrayList<Object>();
			parsedString = p.parseTest("   show     ((a|b)=>c)   ");
			reason = (String) parsedString.get(0);
			expr = (String) parsedString.get(1);
			lines = (ArrayList<LineNumber>) parsedString.get(2);
			
			assertEquals("show", reason);
			assertEquals("((a|b)=>c)", expr.toString());
		} catch (IllegalLineException e) {
            System.out.println (e.getMessage());
		}
		
		//test parsing of print command
		try{
			parsedString = new ArrayList<Object>();
			parsedString = p.parseTest("   print   ");
			assertNull(parsedString);
		} catch (IllegalLineException e) {
            fail(e.getMessage());
		}
		
		//test wrong user inputs
		// #1
		try{
			parsedString = new ArrayList<Object>();
			parsedString = p.parseTest("   show   f  ((a|b)=>c)   ");
			reason = (String) parsedString.get(0);
			expr = (String) parsedString.get(1);
			lines = (ArrayList<LineNumber>) parsedString.get(2);
			
			assertEquals("show", reason);
			assertEquals("((a|b)=>c)", expr.toString());
		} catch (IllegalLineException e) {
            System.out.println (e.getMessage());
		}
		
		// #2
		try{
			parsedString = new ArrayList<Object>();
			parsedString = p.parseTest("   mp((a|b)=>c)   ");
			reason = (String) parsedString.get(0);
			expr = (String) parsedString.get(1);
			lines = (ArrayList<LineNumber>) parsedString.get(2);
			
			assertEquals("show", reason);
			assertEquals("((a|b)=>c)", expr.toString());
		} catch (IllegalLineException e) {
            System.out.println (e.getMessage());
		}
		
		// #3 : handle invalid string LineNumber 
		try{
			parsedString = new ArrayList<Object>();
			parsedString = p.parseTest("mp 5.5 4. (((p=>q)=>q)=>((q=>p)=>p))");
			reason = (String) parsedString.get(0);
			expr = (String) parsedString.get(1);
			lines = (ArrayList<LineNumber>) parsedString.get(2);
			
			assertEquals("show", reason);
			assertEquals("((a|b)=>c)", expr.toString());
		} catch (IllegalLineException e) {
            System.out.println (e.getMessage());
		}
		
		// #4 : types for string LineNumber
		try{
			parsedString = new ArrayList<Object>();
			parsedString = p.parseTest("mp a.b c.d (((p=>q)=>q)=>((q=>p)=>p))");
			reason = (String) parsedString.get(0);
			expr = (String) parsedString.get(1);
			lines = (ArrayList<LineNumber>) parsedString.get(2);
			
			assertEquals("show", reason);
			assertEquals("((a|b)=>c)", expr.toString());
		} catch (IllegalLineException e) {
            System.out.println (e.getMessage());
		}
	}
	
	
	
	// Test that the incorrect number of LineNumbers in
	// the input will cause an IllegalLineException to
	// be thrown. 
	// Test that correct number of LineNumbers does not
	// cause an IllegalLineException to be thrown.
//	If this test were to fail, we would need to look at
//	the exception handling regarding line numbers
	@Test
	public void testCheckLineNumbers1(){
		TheoremSet t = new TheoremSet();
		try{
			t.put("theorem_name", new Expression("(a=>a)"));
		} catch (IllegalLineException ex){
			fail("threw Illegal Line: "+ex.getMessage());
		}
		Proof p = new Proof(t);
		//create an array of test inputs, and a parallel
		//array of booleans that indicate whether the
		//corresponding input should succeed or throw an error
		ArrayList<String> inputs = new ArrayList<String>();
		ArrayList<Boolean> valid = new ArrayList<Boolean>();
		//create valid lines in proof
		inputs.add("show (p=>(~p=>q))"); valid.add(true);	//1
		inputs.add("assume p"); valid.add(true);			//2
		inputs.add("show (~p=>q)"); valid.add(true);		//3
		inputs.add("assume ~p"); valid.add(true);			//3.1
		inputs.add("co 2 3.1 (p=>q)"); valid.add(true);		//3.2
		//test valid inputs
		inputs.add("show (p=>q)"); valid.add(true);
		inputs.add("show 2 (p=>q)"); valid.add(false);
		inputs.add("show 3.1 2 (p=>q)"); valid.add(false);
		inputs.add("assume ~(p=>q)"); valid.add(true);
		inputs.add("assume 3.1 ~(p=>q)"); valid.add(false);
		inputs.add("assume 3.2 2 ~(p=>q)"); valid.add(false);
		inputs.add("mp (p=>q)"); valid.add(false);
		inputs.add("mp 2 (p=>q)"); valid.add(false);
		inputs.add("mp 3.2 2 q"); valid.add(true);
		inputs.add("mt (p=>q)"); valid.add(false);
		inputs.add("mt 2 (p=>q)"); valid.add(false);
		inputs.add("mt 2 3.2 (p=>q)"); valid.add(true);
		inputs.add("co (p=>q)"); valid.add(false);
		inputs.add("co 3.2 (p=>q)"); valid.add(false);
		inputs.add("co 3.2 3.1 (p=>q)"); valid.add(true);
		inputs.add("ic (p=>q)"); valid.add(false);
		inputs.add("ic 2 (p=>q)"); valid.add(true);
		inputs.add("ic 3.1 2  (p=>q)"); valid.add(false);
		inputs.add("repeat (p=>q)"); valid.add(false);
		inputs.add("repeat 2 (p=>q)"); valid.add(true);
		inputs.add("repeat 2 3.2 (p=>q)"); valid.add(false);
		inputs.add("theorem_name (p=>q)"); valid.add(true);
		inputs.add("theorem_name 3.2 (p=>q)"); valid.add(false);
		inputs.add("theorem_name 2 3.1 (p=>q)"); valid.add(false);
		
		
		for (int i=0; i<inputs.size(); i++)
		{
			String input = inputs.get(i);
			boolean isValid = valid.get(i);
			try{
				p.extendProof(input);
				if (!isValid)
					fail(input+" did not throw IllegalLineException");
			} catch (IllegalLineException ex){
				if (isValid)
					fail(input+" threw IllegalLineException");
			} catch (IllegalInferenceException ex){
				if (!isValid)
					fail(input+" threw IllegalInferenceException");
			}
		}
	}
	
	
	// test that inaccessible or nonexistent LineNumbers cause an
	// IllegalLineException to be thrown
//	if these tests were to fail, we would need to look at the
//	implementation of IllegalLineException regarding line numbers
	@Test
	public void testCheckLineNumbers2(){
		Proof p = new Proof(new TheoremSet());
		ArrayList<String> inputs = new ArrayList<String>();
		ArrayList<Boolean> valid = new ArrayList<Boolean>();
		inputs.add("show (p=>(~p=>q))"); valid.add(true);	//1
		inputs.add("assume p"); valid.add(true);			//2
		inputs.add("show (~p=>q)"); valid.add(true);		//3
		inputs.add("assume ~p"); valid.add(true);			//3.1
		inputs.add("co 2 3.1 (~p=>q)"); valid.add(true);	//3.2
		inputs.add("co 2 3.1 (~p=>q)"); valid.add(false);	//line 3.1 is no longer accessible
		inputs.add("co 2 3.3 (~p=>q)"); valid.add(false);	//line 3.3 doesn't exist
		inputs.add("co 2 4 (~p=>q)"); valid.add(false);		//line 4 doesn't exist
		inputs.add("ic 3 (p=>(~p=>q))"); valid.add(true);
		
		for (int i=0; i<inputs.size(); i++)
		{
			String input = inputs.get(i);
			boolean isValid = valid.get(i);
			try{
				p.extendProof(input);
				if (!isValid)
					fail(input+" did not throw IllegalLineException");
			} catch (IllegalLineException ex){
				if (isValid)
					fail(input+" threw IllegalLineException");
			} catch (IllegalInferenceException ex){
				fail(input+" threw IllegalInferenceException");
			}
		}
		
		assertTrue(p.isComplete());
	}
	
	
	// test reason "show" 
//	if this test were to fail, we would need to examine how reason "show"
//	is evaluated and under which conditions it would throw an exception
	@Test
	public void testShow(){
		Proof p = new Proof(new TheoremSet());
		//create an array of test inputs, and a parallel
		//array of booleans that indicate whether the
		//corresponding input should succeed or throw an error
		ArrayList<String> inputs = new ArrayList<String>();
		ArrayList<Boolean> valid = new ArrayList<Boolean>();
		inputs.add("show ((p=>q)=>(~q=>~p))"); valid.add(true); //1 first show
		inputs.add("assume (p=>q)"); valid.add(true);			//2
		inputs.add("show (~q=>~p)"); valid.add(true);			//3 show after assume
		inputs.add("show (~q=>~p)"); valid.add(true);			//3.1 show after show
		inputs.add("assume ~q"); valid.add(true);				//3.1.1
		inputs.add("mp 3.1.1 3.1 ~p"); valid.add(false);		//previous show can't be used until proven
		inputs.add("mp 3 3.1.1 ~p"); valid.add(false);			//earlier show can't be used until proven
		inputs.add("mp 2 1 (~q=>~p)"); valid.add(false);		//first show can't be used
		inputs.add("mt 2 3.1.1 ~p"); valid.add(true);			//3.1.2
		inputs.add("show (q=>q)"); valid.add(true);				//3.1.3 show after other reason
		inputs.add("assume q"); valid.add(true);				//3.1.3.1
		inputs.add("ic 3.1.3.1 (q=>q)"); valid.add(true);		//3.1.3.2
		inputs.add("ic 3.1.2 (~q=>~p)"); valid.add(true);		//3.1.4
		inputs.add("repeat 3.1 (~q=>~p)"); valid.add(true);		//3.2 proved show can be used
		inputs.add("mt 2 3.1.1 ~p"); valid.add(false);			//statement in subproof can't be used
		inputs.add("ic 3 ((p=>q)=>(~q=>~p))"); valid.add(true);	//4
		
		for (int i=0; i<inputs.size(); i++)
		{
			String input = inputs.get(i);
			boolean isValid = valid.get(i);
			try{
				p.extendProof(input);
				if (!isValid)
					fail(input+" did not throw IllegalInferenceException");
			} catch (IllegalLineException ex){
				if(isValid)
					fail(input+" threw IllegalLineException");
			} catch (IllegalInferenceException ex){
				fail(input+" threw IllegalInferenceException");
			}
		}
		
		assertTrue(p.isComplete());
		
	}
	
//	test reason assume
//	if this test were to fail, we would need to examine how reason "assume"
//	is evaluated and under which conditions it would throw an exception
	@Test
	public void testAssume(){
		Proof p = new Proof(null);
		ArrayList<String> inputs = new ArrayList<String>();
		ArrayList<Boolean> valid = new ArrayList<Boolean>();
		
		inputs.add("show (p=>((p=>q)=>q))"); valid.add(true);	//1
		inputs.add("assume p"); valid.add(true);				//2 assume after show
		inputs.add("assume p"); valid.add(false);				// assume after assume
		inputs.add("show ((p=>q)=>q)"); valid.add(true);		//3
		inputs.add("assume q"); valid.add(false);				//can't assume consequent
		inputs.add("assume ((p=>q)=>q)"); valid.add(false);		//can only assume antecedent of show
		inputs.add("assume (p=>q)"); valid.add(true);			//3.1
		inputs.add("mp 2 3.1 q"); valid.add(true);				//3.2
		inputs.add("assume (p=>q)"); valid.add(false);			//can't assume after reason other than show
		inputs.add("show ~(a&b)"); valid.add(true);				//3.3
		inputs.add("assume (a&b)"); valid.add(false);			//can only assume ~E
		inputs.add("assume ~~(a&b)"); valid.add(true);			//3.3 assume negation		
	
		for (int i=0; i<inputs.size(); i++)
		{
			String input = inputs.get(i);
			boolean isValid = valid.get(i);
			try{
				p.extendProof(input);
				if (!isValid)
					fail(input+" did not throw IllegalInferenceException");
			} catch (IllegalLineException ex){
				fail(input+" threw IllegalLineException");
			} catch (IllegalInferenceException ex){
				if (isValid)
					fail(input+" threw IllegalInferenceException");
			}
		}
	}
	
//	tests mp
//	if this test were to fail, we would need to examine the
//	implementation of modus ponens and check under which
//	conditions it would throw an exception
	@Test
	public void testModusPonens(){
		Proof p = new Proof(null);
		ArrayList<String> inputs = new ArrayList<String>();
		ArrayList<Boolean> valid = new ArrayList<Boolean>();
		
		inputs.add("show (p=>((p=>q)=>q))"); valid.add(true);	//1
		inputs.add("assume p"); valid.add(true);				//2
		inputs.add("show ((p=>q)=>q)"); valid.add(true);		//3
		inputs.add("assume (p=>q)"); valid.add(true);			//3.1
		inputs.add("mp 2 3.1 q"); valid.add(true);				//3.2
		inputs.add("mp 3.1 2 q"); valid.add(true);				//3.3
		inputs.add("mp 3.2 3.1 p"); valid.add(false);			//q doesn't imply p
		inputs.add("mp 3.1 3.2 p"); valid.add(false);			//q doesn't imply p
		inputs.add("show ((p&q)=>p)"); valid.add(true);			//3.4		
		inputs.add("assume (p&q)"); valid.add(true);			//3.4.1
		inputs.add("mp 2 3.4.1 q"); valid.add(false);			//mp only works with =>
		
		
		for (int i=0; i<inputs.size(); i++)
		{
			String input = inputs.get(i);
			boolean isValid = valid.get(i);
			try{
				p.extendProof(input);
				if (!isValid)
					fail(input+" did not throw IllegalInferenceException");
			} catch (IllegalLineException ex){
				fail(input+" threw IllegalLineException");
			} catch (IllegalInferenceException ex){
				if (isValid)
					fail(input+" threw IllegalInferenceException");
			}
		}
		
	}
	
//	test modus tollens
//	if this test were to fail, we would need to examine the
//	impelementation of modus tollens and check under which
//	conditions it throws and exception
	@Test
	public void testModusTollens(){
		Proof p = new Proof(null);
		ArrayList<String> inputs = new ArrayList<String>();
		ArrayList<Boolean> valid = new ArrayList<Boolean>();
		
		inputs.add("show (~q=>((p=>q)=>q))"); valid.add(true);	//1
		inputs.add("assume ~q"); valid.add(true);				//2
		inputs.add("show ((p=>q)=>q)"); valid.add(true);		//3
		inputs.add("assume (p=>q)"); valid.add(true);			//3.1
		inputs.add("mt 2 3.1 ~p"); valid.add(true);				//3.2
		inputs.add("mt 3.1 2 ~p"); valid.add(true);				//3.3
		inputs.add("mt 3.2 3.1 ~q"); valid.add(false);			//~p doesn't imply ~q
		inputs.add("mt 3.1 3.2 ~q"); valid.add(false);			//~p doesn't imply ~q
		inputs.add("show ((p&q)=>p)"); valid.add(true);			//3.4
		inputs.add("assume (p&q)"); valid.add(true);			//3.4.1
		inputs.add("mp 2 3.4.1 ~p"); valid.add(false);			//mt only works with =>
		
		
		for (int i=0; i<inputs.size(); i++)
		{
			String input = inputs.get(i);
			boolean isValid = valid.get(i);
			try{
				p.extendProof(input);
				if (!isValid)
					fail(input+" did not throw IllegalInferenceException");
			} catch (IllegalLineException ex){
				fail(input+" threw IllegalLineException");
			} catch (IllegalInferenceException ex){
				if (isValid)
					fail(input+" threw IllegalInferenceException");
			}
		}
	}
	
//	test contradiction
//	if this test were to fail, we would need to examine the
//	implementation of negations and contradictions and check
//	under which conditions it throws an excpetion
	@Test
	public void testContradiction(){
		Proof p = new Proof(null);
		ArrayList<String> inputs = new ArrayList<String>();
		ArrayList<Boolean> valid = new ArrayList<Boolean>();
		
		inputs.add("show (p=>(~p=>q))"); valid.add(true);		//1
		inputs.add("assume p"); valid.add(true);				//2
		inputs.add("show (~p=>q)"); valid.add(true);			//3
		inputs.add("assume ~p"); valid.add(true);				//3.1
		inputs.add("show (~~~p=>~~~p)"); valid.add(true);		//3.2
		inputs.add("assume ~~~p"); valid.add(true);				//3.2.1
		inputs.add("co 2 3.2.1 ~~p"); valid.add(false);			//contradiction must be E and ~E
		inputs.add("ic 3.2.1 (~~~p=>~~~p)"); valid.add(true);	//3.2.2
		inputs.add("co 3.1 2 ((a&b)|c)"); valid.add(true);		//3.3 can infer anything with contradiction
		inputs.add("co 2 3.1 (~p=>q)"); valid.add(true);		//3.4 use contradiction in opposite order
		inputs.add("ic 3 (p=>(~p=>q))"); valid.add(true);
		
		for (int i=0; i<inputs.size(); i++)
		{
			String input = inputs.get(i);
			boolean isValid = valid.get(i);
			try{
				p.extendProof(input);
				if (!isValid)
					fail(input+" did not throw IllegalInferenceException");
			} catch (IllegalLineException ex){
				fail(input+" threw IllegalLineException");
			} catch (IllegalInferenceException ex){
				if (isValid)
					fail(input+" threw IllegalInferenceException");
			}
		}
	}
	
//	tests ic
//	if this test were to fail, we would need to examine the
//	implementation of implication construction and check the
//	conditions under which it would throw an exception
	@Test
	public void testImplicationConstruction(){
		Proof p = new Proof(null);
		ArrayList<String> inputs = new ArrayList<String>();
		ArrayList<Boolean> valid = new ArrayList<Boolean>();
		
		inputs.add("show (p=>q)"); valid.add(true);			//1
		inputs.add("assume p"); valid.add(true);			//2
		inputs.add("ic 2 (p=>q)"); valid.add(false);		//implication is reversed
		inputs.add("ic 2 (q=>p)"); valid.add(true);			//3 correct implication
		inputs.add("ic 2 ((a|~b)=>p)"); valid.add(true);	//4 can assume anything implies p
		
		for (int i=0; i<inputs.size(); i++)
		{
			String input = inputs.get(i);
			boolean isValid = valid.get(i);
			try{
				p.extendProof(input);
				if (!isValid)
					fail(input+" did not throw IllegalInferenceException");
			} catch (IllegalLineException ex){
				fail(input+" threw IllegalLineException");
			} catch (IllegalInferenceException ex){
				if (isValid)
					fail(input+" threw IllegalInferenceException");
			}
		}
	}
	
//	test repeat
//	if this test were to fail, we would need to examine when repeat
//	throws an exception and when it is valid to repeat a line
	@Test
	public void testRepeat(){
		Proof p = new Proof(null);
		ArrayList<String> inputs = new ArrayList<String>();
		ArrayList<Boolean> valid = new ArrayList<Boolean>();
		
		inputs.add("show (p=>q)"); valid.add(true);			//1
		inputs.add("assume p"); valid.add(true);			//2
		inputs.add("repeat 2 p"); valid.add(true);			//3 repeat
		inputs.add("repeat 3 p"); valid.add(true);			//4 repeat a repeat
		inputs.add("show (q=>p)"); valid.add(true);			//5
		inputs.add("repeat 5 (q=>p)"); valid.add(false);	// can't repeat unproven show
		inputs.add("ic 2 (q=>p)"); valid.add(true);			//5.1
		inputs.add("repeat 5 (q=>p)"); valid.add(true);		//can repeat proven show
		
		
		for (int i=0; i<inputs.size(); i++)
		{
			String input = inputs.get(i);
			boolean isValid = valid.get(i);
			try{
				p.extendProof(input);
				if (!isValid)
					fail(input+" did not throw IllegalInferenceException");
			} catch (IllegalLineException ex){
				if (isValid)
					fail(input+" threw IllegalLineException");
			} catch (IllegalInferenceException ex){
				fail(input+" threw IllegalInferenceException");
			}
		}
	}
	
//	tests toString method
//	if this test were to fail, we would need to look at the toString method
//	in both extendProof and Expression
	@Test
	public void testToString(){
		Proof p = new Proof(null);
		
		try{
			assertEquals("", p.toString());
			p.extendProof("show (~~p=>p)");
			assertEquals("\n1\tshow (~~p=>p)", p.toString());
			p.extendProof("assume ~~p");
			assertEquals("\n1\tshow (~~p=>p)\n2\tassume ~~p", p.toString());
			p.extendProof("show p");
			p.extendProof("assume ~p");
			assertEquals("\n1\tshow (~~p=>p)\n2\tassume ~~p\n3\tshow p\n3.1\tassume ~p", p.toString());
			try{
				p.extendProof("co 2 3.2 p");
			} catch (IllegalLineException ex){
				assertEquals("\n1\tshow (~~p=>p)\n2\tassume ~~p\n3\tshow p\n3.1\tassume ~p", p.toString());
			}
			try{
				p.extendProof("mp 2 3.1 p");
			} catch (IllegalInferenceException ex){
				assertEquals("\n1\tshow (~~p=>p)\n2\tassume ~~p\n3\tshow p\n3.1\tassume ~p", p.toString());
			}
			p.extendProof("show ~p");
			p.extendProof("repeat 3.1 ~p");
			p.extendProof("co 2 3.1 p");
			assertEquals("\n1\tshow (~~p=>p)\n2\tassume ~~p\n3\tshow p\n3.1\tassume ~p\n3.2\tshow ~p\n3.2.1\trepeat 3.1 ~p\n3.3\tco 2 3.1 p", p.toString());
			p.extendProof("ic 3 (~~p=>p)");
			assertEquals("\n1\tshow (~~p=>p)\n2\tassume ~~p\n3\tshow p\n3.1\tassume ~p\n3.2\tshow ~p\n3.2.1\trepeat 3.1 ~p\n3.3\tco 2 3.1 p\n4\tic 3 (~~p=>p)", p.toString());
			
			
		} catch (IllegalInferenceException ex){
			fail("threw Illegal Inference: "+ex.getMessage());
		} catch (IllegalLineException ex){
			fail("threw Illegal Line: "+ex.getMessage());
		}
	}
	
//	tests recognition of theorem names 
//	if this were to fail, we would need to look at the way we evaluate
//	theorem names in extend proof.
	@Test
	public void testTheoremUse(){
		TheoremSet t = new TheoremSet();
		try {
			t.put("dn", new Expression("(~~a=>a)"));
		} catch (IllegalLineException ex){
			fail("threw IllegalLine: "+ex.getMessage());
		}
		Proof p = new Proof(t);
		try {
			p.extendProof("show (a=>~~a)");
			p.extendProof("assume a");
			p.extendProof("show ~~a");
			p.extendProof("assume ~~~a");
			try{
				p.extendProof("nd (~~~a=>~a)");		//misspelled theorem name
				fail("did not throw IllegalLineException");
			}catch (IllegalLineException ex){
			}
			try{
				p.extendProof("dn (~~a=>~a)");		//incorrect use of theorem
				fail("did not throw IllegalInferenceException");
			}catch (IllegalInferenceException ex){
			}
			p.extendProof("dn (~~~a=>~a)");			//correct use of theorem
			p.extendProof("mp 3.2 3.1 ~a");			//use of statement derived from theorem
		} catch (IllegalInferenceException ex){
			fail("threw Illegal Inference: "+ex.getMessage());
		} catch (IllegalLineException ex){
			fail("threw Illegal Line: "+ex.getMessage());
		}
	}
	
//	tests line numbering
//	if this test were to fail, we would need to look at how line numbers are
//	incremented within extend proof
	@Test
	public void testNextLineNumber(){
		Proof p = new Proof(new TheoremSet());
		try{
			assertEquals("1", p.nextLineNumber().toString());
			p.extendProof("show (((p=>q)=>q)=>((q=>p)=>p))");
			assertEquals("2", p.nextLineNumber().toString());
			p.extendProof("assume ((p=>q)=>q)");
			assertEquals("3", p.nextLineNumber().toString());
			p.extendProof("show ((q=>p)=>p)");
			assertEquals("3.1", p.nextLineNumber().toString());
			p.extendProof("assume (q=>p)");
			assertEquals("3.2", p.nextLineNumber().toString());
			p.extendProof("show p");
			assertEquals("3.2.1", p.nextLineNumber().toString());
			p.extendProof("assume ~p");
			assertEquals("3.2.2", p.nextLineNumber().toString());
			p.extendProof("mt 3.2.1 3.1 ~q");
			assertEquals("3.2.3", p.nextLineNumber().toString());
			p.extendProof("mt 2 3.2.2 ~(p=>q)");
			assertEquals("3.2.4", p.nextLineNumber().toString());
			p.extendProof("show (p=>q)");
			assertEquals("3.2.4.1", p.nextLineNumber().toString());
			try{
				p.extendProof("assume 2 p");
			}catch(IllegalLineException ex){
				assertEquals("3.2.4.1", p.nextLineNumber().toString());
			}
			p.extendProof("assume p");
			assertEquals("3.2.4.2", p.nextLineNumber().toString());
			p.extendProof("co 3.2.4.1 3.2.1 (p=>q)");
			assertEquals("3.2.5", p.nextLineNumber().toString());
			try{
				p.extendProof("mp 3.2.4 3.2.3 p");
			}catch(IllegalInferenceException ex){
				assertEquals("3.2.5", p.nextLineNumber().toString());
			}
			p.extendProof("co 3.2.4 3.2.3 p");
			assertEquals("3.3", p.nextLineNumber().toString());
			p.extendProof("ic 3.2 ((q=>p)=>p)");
			assertEquals("4", p.nextLineNumber().toString());
			p.extendProof("ic 3 (((p=>q)=>q)=>((q=>p)=>p))");
			assertTrue(p.isComplete());
		} catch (IllegalInferenceException ex){
			fail("threw Illegal Inference: "+ex.getMessage());
		} catch (IllegalLineException ex){
			fail("threw Illegal Line: "+ex.getMessage());
		}
	}
	
//	tests isComplete method
//	if this test were to fail, we would need to examine the manner
//	in which we detect whether a proof is complete or not within
//	extend proof.
	@Test
	public void testIsComplete(){
		Proof p = new Proof(new TheoremSet());
		
		try{
			assertFalse(p.isComplete());
			p.extendProof("show (p=>((p=>q)=>q))");
			assertFalse(p.isComplete());
			p.extendProof("assume p");
			assertFalse(p.isComplete());
			p.extendProof("show ((p=>q)=>q)");
			assertFalse(p.isComplete());
			p.extendProof("assume (p=>q)");
			assertFalse(p.isComplete());
			try{
				p.extendProof("assume ((p=>q)=>q)");
			}catch(IllegalInferenceException ex){
				assertFalse(p.isComplete());
			}
			try{
				p.extendProof("assume 2 ((p=>q)=>q)");
			}catch(IllegalLineException ex){
				assertFalse(p.isComplete());
			}
			p.extendProof("show q");
			p.extendProof("mp 2 3.1 q");
			p.extendProof("ic 3.2 ((p=>q)=>q)");
			assertFalse(p.isComplete());
			p.extendProof("ic 3 (p=>((p=>q)=>q))");
			assertTrue(p.isComplete());
		} catch (IllegalInferenceException ex){
			fail("threw Illegal Inference: "+ex.getMessage());
		} catch (IllegalLineException ex){
			fail("threw Illegal Line: "+ex.getMessage());
		}
	}
	
	// tests that a proof can be completed with print
	// commands given at various points
	@Test
	public void testPrint(){
		Proof p = new Proof(new TheoremSet());
		//create an array of test inputs, and a parallel
		//array of booleans that indicate whether the
		//corresponding input should succeed or throw an error
		ArrayList<String> inputs = new ArrayList<String>();
		ArrayList<Boolean> valid = new ArrayList<Boolean>();
		inputs.add("print"); valid.add(true);					//print as first input
		inputs.add("show ((p=>q)=>(~q=>~p))"); valid.add(true); //1
		inputs.add("print"); valid.add(true);					//print after first input
		inputs.add("assume (p=>q)"); valid.add(true);			//2
		inputs.add("show (~q=>~p)"); valid.add(true);			//3
		inputs.add("print"); valid.add(true);					//print before any subproofs
		inputs.add("show (~q=>~p)"); valid.add(true);			//3.1
		inputs.add("print"); valid.add(true);					//print after first line of subproof
		inputs.add("assume ~q"); valid.add(true);				//3.1.1
		inputs.add("mp 3.1.1 3.1 ~p"); valid.add(false);		//invalid input
		inputs.add("print"); valid.add(true);					//print after invalid input
		inputs.add("mt 2 3.1.1 ~p"); valid.add(true);			//3.1.2
		inputs.add("show (q=>q)"); valid.add(true);				//3.1.3 
		inputs.add("assume q"); valid.add(true);				//3.1.3.1
		inputs.add("print"); valid.add(true);					//print in nested subproofs
		inputs.add("ic 3.1.3.1 (q=>q)"); valid.add(true);		//3.1.3.2
		inputs.add("ic 3.1.2 (~q=>~p)"); valid.add(true);		//3.1.4
		inputs.add("print"); valid.add(true);					//print in nested subproofs
		inputs.add("repeat 3.1 (~q=>~p)"); valid.add(true);		//3.2 
		inputs.add("print"); valid.add(true);					//print after completed subproofs
		inputs.add("ic 3 ((p=>q)=>(~q=>~p))"); valid.add(true);	//4
		
		for (int i=0; i<inputs.size(); i++)
		{
			String input = inputs.get(i);
			boolean isValid = valid.get(i);
			try{
				p.extendProof(input);
				if (!isValid)
					fail(input+" did not throw IllegalInferenceException");
			} catch (IllegalLineException ex){
				if(isValid)
					fail(input+" threw IllegalLineException");
			} catch (IllegalInferenceException ex){
				fail(input+" threw IllegalInferenceException");
			}
		}
		
		assertTrue(p.isComplete());
		
	}

}
