public class BinaryTree {

	protected TreeNode myRoot;
	
	public BinaryTree ( ) {
		myRoot = null;
	}
	
	public BinaryTree (TreeNode t) {
		myRoot = t;
	}
	
	public TreeNode getRoot() {
		return myRoot;
	}
	
	public int size( ) {
		if(myRoot != null)
			return sizeHelper(myRoot);
		return 0;
	}
	
	private static int sizeHelper(TreeNode t) {
		int left = 0;
		int right = 0;
		if(t.myItem == null)
			return 0;
		if(t.myLeft != null)
			left = BinaryTree.sizeHelper(t.myLeft);
		if(t.myRight != null)
			right = BinaryTree.sizeHelper(t.myRight);
		return 1 + left + right;
	}
	
	// returns true if this and other represent
	// equivalent logical expressions
	public boolean equals(Object tree){
		try{
			BinaryTree bTree = ((BinaryTree)tree);
			return equals(bTree);
		} catch (ClassCastException ex){
			return false;
		}
	}

	public boolean equals(BinaryTree a) {
		if(a == null) {
			return false;
		}
		return BinaryTree.equalsHelper(myRoot, a.myRoot);
	}

	private static boolean equalsHelper(TreeNode a, TreeNode b) {
		if(a == null && b == null)
			return true;
		else if(a == null || b == null)
			return false;
		else if(!(a.myItem.equals(b.myItem))) {
			return false;
		}
		return BinaryTree.equalsHelper(a.myLeft, b.myLeft) && BinaryTree.equalsHelper(a.myRight, b.myRight);
	}
	
	public void print ( ) {
	    if (myRoot != null) {
	        printHelper (myRoot, 0);
	    }
	}
		
	private static void printHelper (TreeNode node, int indent) {
		if(node.myRight != null){
			printHelper(node.myRight, indent + 1);
		}
		if(node.myItem != null){
			println (node.myItem, indent);
		}
	    if(node.myLeft != null){
	    	printHelper(node.myLeft, indent + 1);
	    }
	}
	
	private static final String indent1 = "    ";
			
	private static void println (Object obj, int indent) {
	    for (int k=0; k<indent; k++) {
	        System.out.print (indent1);
	    }
	    System.out.println (obj);
	}
	
	//Return string representation of tree that can be 
	//given as argument to BinaryTree.exprTree to 
	//create an identical tree	
	public String getExpString(){
		if (myRoot == null)
			return "";
		else
			return myRoot.toString();
	}
}
	
