import junit.framework.TestCase;

//tests the functionality of TheoremSet
//if any of these tests were to fail, we would need to look at
//the implementation of the respective attributes of TheoremSet
//We would need to examine both TheoremSet and Expression exception
//handling if these tests were to fail
public class TheoremSetTest extends TestCase {

//	tests the functionality of the hash table within TheoremSet
	public void testPut( ) {
		TheoremSet test = new TheoremSet( );
		try {
			String name = "and1";
			Expression expr = new Expression("((x&y)=>x)");
			test.put(name, expr);
			assertTrue(test.mySet.containsKey("and1"));
			assertFalse(test.mySet.containsKey("and2"));
			assertEquals(test.mySet.get("and1").toString( ), "((x&y)=>x)");
		}
		catch (IllegalLineException ile) {
			System.err.println(ile.getMessage( ));
		}
	}
	
//	tests that an exception is thrown when an invalid (null) expression
//	name is given
	public void testNullName( ) {
		TheoremSet test = new TheoremSet( );
		boolean output = false;
		try {
			String name = null;
			Expression expr = new Expression("((x&y)=>x)");
			test.put(name, expr);
		}
		catch (IllegalLineException ile) {
			output = true;
		}
		assertTrue(output);
	}
	
//	test that an exception is thrown when an invalid (empty string)
//	expression name is given
	public void testEmptyName( ) {
		TheoremSet test = new TheoremSet( );
		boolean output = false;
		try {
			String name = "";
			Expression expr = new Expression("((x&y)=>x)");
			test.put(name, expr);
		}
		catch (IllegalLineException ile) {
			output = true;
		}
		assertTrue(output);
	}
	
//	tests that an exception is thrown when an invalid expression is given
	public void testNullExpr( ) {
		TheoremSet test = new TheoremSet( );
		boolean output = false;
		try {
			String name = "and1";
			Expression expr = null;
			test.put(name, expr);
		}
		catch (IllegalLineException ile) {
			output = true;
		}
		assertTrue(output);
	}
}