import java.util.*;

public class Expression {

	private BinaryTree myTree;
	private String myString;

	private static final Set<String> SYMBOLS = new HashSet<String>(Arrays.asList(new String[] {"=>","&","|","~"}));

	// Constructs an Expression from a String
	public Expression (String s) throws IllegalLineException {
		myTree = new BinaryTree(Expression.exprHelper(s));
		myString = s;
		
		checkParentheses(s);
		
		int var = 0;

		for (int i = 0; i < myString.length()-1; i++) {
			if (var < 0)
				var = 0;
			if (Character.isLetter(myString.charAt(i)))
				var ++; 
			else
				var --;
			if (var > 1)
				throw new IllegalLineException ("*** variables may only be one character: " + myString);
			if (myString.charAt(i) == ' ')
				throw new IllegalLineException ("*** expressions cannot have spaces: " + myString);
		}
		
		
		try{
			isOK(s);
		} catch (IllegalLineException ex){
			throw new IllegalLineException("*** Invalid Expression: "+s);
		}
	}
	
	/* 
	 * Constructs an Expression from a TreeNode by making the TreeNode
	 * the root of the Expression's BinaryTree
	 */
	public Expression(TreeNode node){
		myTree = new BinaryTree(node);
		myString = myTree.getExpString();
	}
	
	/*
	 * Checks that all expressions are surrounded by parentheses. The exception
	 * is if the expression contains only a single variable, in which case it
	 * throws an exception if the expression is surrounded by parentheses.
	 */
	private static void checkParentheses(String s) throws IllegalLineException {
		int count = 0;

		if (s.charAt(0) == '(' && s.charAt(s.length()-1)!=')')
			throw new IllegalLineException("*** expression needs parentheses: " + s);
		if (s.charAt(0) != '(' && s.charAt(s.length()-1)==')')
			if(s.charAt(0) != '~')
				throw new IllegalLineException("*** expression needs parentheses: " + s);
		
		for (int i = 0; i < s.length()-1; i++)
			if (s.charAt(i) != '~')
				count++;
		
		if (s.charAt(0) != '(' && s.charAt(s.length()-1) != ')') {
			if (count > 1)
				throw new IllegalLineException ("*** expression needs parentheses: " + s);
		}
		
		if ((s.charAt(0) == '(') && (s.charAt(s.length( )-1) == ')') && (s.length( ) < 5))
			throw new IllegalLineException ("*** no parentheses around variable: " + s);
	}

	// Checks that the given String can be formed into a legal expression
	// If it can't, throws an IllegalLineException
	public void isOK(String s) throws IllegalLineException{
		Stack<Character> tokens = new Stack<Character>();
		// pushes each character in the string onto the stack
		for (int i=0; i<s.length(); i++){
			char c = s.charAt(i);
			// if the character is '=', check that it is followed by '>' 
			if(c == '='){
				try{
					if (s.charAt(i+1) != '>'){
						throw new IllegalLineException("= without >");
					} else {
						//skip the '>'
						i++;
					}
				} catch (IndexOutOfBoundsException ex){
					throw new IllegalLineException("= at end");
				}
			}
			push(tokens, c);
		}
		
		// if after pushing all the characters onto the stack, it
		// contains anything other than a single letter, the 
		// expression was invalid
		if (tokens.size() != 1){
			throw new IllegalLineException("stack not 1: "+tokens);
		}
		if (!Character.isLowerCase(tokens.peek())){
			throw new IllegalLineException("not an expression: "+tokens);
		}
	}
	
	//helper method for isOK to push characters onto stack
	private void push(Stack<Character> s, char c) throws IllegalLineException{
		try{
			if (c == ')'){
				// remove the top four elements in the stack
				char exp2 = s.pop();
				char op = s.pop();
				char exp1 = s.pop();
				char paren = s.pop();
				// check that they form a valid expression of the form (a?b)
				if (!Character.isLowerCase(exp2) || !Character.isLowerCase(exp1) ||
						paren != '(' || (op!='&' && op!='|' && op!='=')){
					throw new IllegalLineException("invalid expression: "+paren+exp1+op+exp2+")");
				}
				// push a letter onto the stack to represent the valid expression
				push(s,exp2);
			} else {
				// if pushing a letter onto the stack, the top of the stack has '~'
				if (!s.isEmpty() && (Character.isLowerCase(c)) && (s.peek() == '~'))
				{
					//remove the '~' before pushing the letter
					s.pop();
					push(s,c);
				} else{
					//base case is a normal Stack.push(char)
					s.push(c);
				}
			} 
		} catch (EmptyStackException ex){
			throw new IllegalLineException("invalid: empty stack: "+c);
		}
	}

	/*	
	 * Return the tree corresponding to the given arithmetic expression.
	 * The expression is legal, fully parenthesized, contains no blanks, 
	 * and involves only the operations +, |, &, ~ and *.
	 */
	private static TreeNode exprHelper (String expr){
		if(expr.equals("")){
			return null;
		}
		if (expr.length() == 1) {
			return new TreeNode(expr); // you fill this in
		} else {
			// expr is a parenthesized expression.
			// Strip off the beginning and ending parentheses,
			// find the main operator (an occurrence of +, |, &, ~ or * not nested
			// in parentheses, and construct the two subtrees.
			int init_len = expr.length();
			expr = expr.replaceAll("^\\(", "");
			if(expr.length() != init_len){
				expr = expr.replaceAll("\\)$", "");
			}
			expr = expr.replace("=>", "*");
			int nesting = 0;
			int opPos = 0;
			for (int k = 0; k<expr.length()-1; k++) {
				if(nesting == 0 && "&|*~".contains(expr.subSequence(k, k + 1)) && k != 0 && !"&|*~".contains(expr.subSequence(k - 1, k))){
					opPos = k;
				}
				else if(expr.charAt(k) == '('){
					nesting++;
				}
				else if(expr.charAt(k) == ')'){
					nesting--;
				}
			}
			String op = expr.substring (opPos, opPos+1);
			String opnd1 = expr.substring (0, opPos);
			String opnd2 = expr.substring (opPos+1, expr.length());

			opnd1 = opnd1.replace("*", "=>");
			opnd2 = opnd2.replace("*", "=>");
			op = op.replace("*", "=>");
			return new TreeNode(op, Expression.exprHelper(opnd1), Expression.exprHelper(opnd2)); // you fill this in
		}
	}

	// Returns a String representation of the Expression
	public String toString(){
		return myString;
	}

	// Returns a BinaryTree representation of the Expression
	public BinaryTree getTree(){
		return myTree;
	}

	// Returns a String representation of the main operator in the Expression
	public String getExpOperator(){
		return (String)myTree.getRoot().getItem();
	}

	// Returns the subexpression to the left of the main operator
	public Expression getLeftExpression(){
		return new Expression(myTree.getRoot().getLeft());
	}

	// Returns the subexpression to the right of the main operator
	public Expression getRightExpression(){
		return new Expression(myTree.getRoot().getRight());
	}

	// Returns true if two Expressions have the same value, false otherwise
	public boolean equals(Object obj){
		if (obj == null)
			return false;
		try{
			Expression exp = (Expression)obj;
			return myTree.equals(exp.myTree);
		} catch (ClassCastException ex){
			return false;
		}
	}

	// Returns true if the Expression is a valid application of the theorem
	public boolean matchesTheorem(Expression theorem) {
		if(myTree.size( ) < theorem.myTree.size( ))
			return false;

		HashMap<String, String> variables = new HashMap<String, String>( );

		return Expression.matchesTheoremHelper(myTree.myRoot, theorem.myTree.myRoot, variables);
	}

	// Helper method for matchesTheorem
	public static boolean matchesTheoremHelper(TreeNode lineNode, TreeNode thrmNode, HashMap<String, String> variables) {
		if(thrmNode == null)
			return true;
		if(SYMBOLS.contains((String)(thrmNode.myItem))) {
			if(!(((String)(thrmNode.myItem)).equals((String)(lineNode.myItem))))
				return false;   
		}
		else {
			String thrmSubexpr = (new BinaryTree(thrmNode)).getExpString( );
			String lineSubexpr = (new BinaryTree(lineNode)).getExpString( );
			if(variables.containsKey(thrmSubexpr)) {
				if(!((lineSubexpr).equals(variables.get(thrmSubexpr))))
					return false;
			}
			else {
				variables.put(thrmSubexpr, lineSubexpr);
				return true;
			}
		}
		return (Expression.matchesTheoremHelper(lineNode.myLeft, thrmNode.myLeft, variables) &&
				Expression.matchesTheoremHelper(lineNode.myRight, thrmNode.myRight, variables));
	}
}
