import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;


public class LineNumberTest {

	/* Invalid arguments are tested in ProofTest file.
	* Each method has one test with multiple sublines with more than tens digit 
	* and one test with a unique number. So all possibilities are tested.
	* It uses a private static helper to test the values in the array for the constructor.
	* The toString method having been tested, we use it to test the other methods.
	*/
	@Test
	public void LineNumberTest() {
		// normal case
		LineNumber line = new LineNumber("1.44.3.51111");
		int[] attended = new int[]{1,44,3,51111};
		assertTrue(arrayTestHelper(attended, line.getArray()));
		
		// unique number
		line = new LineNumber("1");
		attended = new int[]{1};
		assertTrue(arrayTestHelper(attended, line.getArray()));
	}
	
	@Test
	public void toStringTest() {
		//normal case
		LineNumber line = new LineNumber("1.44.3.51111");
		assertEquals("1.44.3.51111", line.toString());
		
		// unique number
		line = new LineNumber("1");
		assertEquals("1", line.toString());
	}
	
	@Test
	public void nextLineTest() {
		//normal case
		LineNumber line = new LineNumber("1.44.3.50");
		line.nextLine();
		assertEquals("1.44.3.51", line.toString());
		
		// unique number
		line = new LineNumber("1");
		line.nextLine();
		assertEquals("2", line.toString());
	}
	
	@Test
	public void nextSubLineTest() {
		//normal case
		LineNumber line = new LineNumber("1.44.3.50");
		line.nextSubLine();
		assertEquals("1.44.3.50.1", line.toString());
		
		// unique number
		line = new LineNumber("1");
		line.nextSubLine();
		assertEquals("1.1", line.toString());
	}
	
	@Test
	public void prevLineTest() {
		//normal case
		LineNumber line = new LineNumber("1.44.3.50");
		line.prevLine();
		assertEquals("1.44.4", line.toString());
		
		// unique number
		try{
			line = new LineNumber("4");
			line.prevLine();
		}
		catch(IllegalStateException e){
			System.out.println(e.getMessage());
		}
	}
	
	private static boolean arrayTestHelper(int[] attended, ArrayList<Integer> toTest){
		int size = toTest.size();
		if(size != attended.length){
			return false;
		}
		int i = 0;
		while(i < toTest.size()){
			if(attended[i] != toTest.get(i)){
				return false;
			}
			i++;
		}
		return true;
	}

}
